<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {font-family: Arial, Helvetica, sans-serif;}
* {box-sizing: border-box;}

input[type=text], select, textarea {
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  margin-top: 6px;
  margin-bottom: 16px;
  resize: vertical;
}

input[type=submit] {
  background-color: #04AA6D;
  color: white;
  padding: 12px 20px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
}

input[type=submit]:hover {
  background-color: #45a049;
}

.container {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}
</style>
</head>
<body>
<style>
div {
  background-image: url('https://t4.ftcdn.net/jpg/01/95/00/93/360_F_195009334_DIJYt241U5FaQeWNS1fU9XFv3WTXM36P.jpg');
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-size: 100% 100%;
}
</style>

<center><h3>Feedback Form</h3></center>

<div class="container">
  <form action='form-submit' method="post">
      @csrf
    <label for="fname">Your  Name</label>
    <input type="text" id="name" name="name" placeholder="Your name.."><br><br><br>

    <label for="fname">EmailId</label>
    <input type="email" id="email" name="emailid" placeholder="Your email.."><br><br><br><br><br><br><br><br><br><br><br><br>


    <label for="subject">Subject</label>
    <textarea id="subject" name="subject" placeholder="Write something.." style="height:200px"></textarea>

    <input type="submit" value="Submit">
  </form>
</div>

</body>
</html>
