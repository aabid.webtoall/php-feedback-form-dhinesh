-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 24, 2021 at 10:59 AM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `feedback`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `feedbackdetails`
--

CREATE TABLE `feedbackdetails` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userfeedback` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `feedbackdetails`
--

INSERT INTO `feedbackdetails` (`id`, `name`, `email`, `userfeedback`, `created_at`, `updated_at`) VALUES
(1, 'ganeshkumar', 'ganesh@gmail.com', 'hdjhdj', '2021-07-24 01:57:03', '2021-07-24 01:57:03'),
(2, 'ganeshkumar', 'ganesh@gmail.com', 'hdjhdj', '2021-07-24 02:07:34', '2021-07-24 02:07:34'),
(3, 'ganeshkumar', 'ganesh@gmail.com', 'hdjhdj', '2021-07-24 02:08:00', '2021-07-24 02:08:00'),
(4, 'ganeshkumar', 'ganesh@gmail.com', 'hdjhdj', '2021-07-24 02:09:05', '2021-07-24 02:09:05'),
(5, 'ganeshkumar', 'ganesh@gmail.com', 'hdjhdj', '2021-07-24 02:09:38', '2021-07-24 02:09:38'),
(6, 'ganeshkumar', 'ganesh@gmail.com', 'hdjhdj', '2021-07-24 02:10:04', '2021-07-24 02:10:04'),
(7, 'ganeshkumar', 'ganesh@gmail.com', 'hdjhdj', '2021-07-24 02:11:46', '2021-07-24 02:11:46'),
(8, 'ganeshkumar', 'ganesh@gmail.com', 'hdjhdj', '2021-07-24 02:12:14', '2021-07-24 02:12:14'),
(9, 'ganeshkumar', 'ganesh@gmail.com', 'hdjhdj', '2021-07-24 02:18:53', '2021-07-24 02:18:53'),
(10, 'ganeshkumar', 'ganesh@gmail.com', 'hdjhdj', '2021-07-24 02:25:27', '2021-07-24 02:25:27'),
(11, 'ganeshkumar', 'ganesh@gmail.com', 'hdjhdj', '2021-07-24 02:26:17', '2021-07-24 02:26:17'),
(12, 'ganeshkumar', 'ganesh@gmail.com', 'hdjhdj', '2021-07-24 02:27:59', '2021-07-24 02:27:59'),
(13, 'ganeshkumar', 'ganesh@gmail.com', 'hdjhdj', '2021-07-24 02:28:17', '2021-07-24 02:28:17'),
(14, 'ganeshkumar', 'ganesh@gmail.com', 'hdjhdj', '2021-07-24 02:31:41', '2021-07-24 02:31:41'),
(15, 'ganeshkumar', 'ganesh@gmail.com', 'hdjhdj', '2021-07-24 02:32:48', '2021-07-24 02:32:48'),
(16, 'ganeshkumar', 'ganesh@gmail.com', 'hdjhdj', '2021-07-24 02:40:20', '2021-07-24 02:40:20'),
(17, 'ganeshkumar', 'ganesh@gmail.com', 'hdjhdj', '2021-07-24 02:40:41', '2021-07-24 02:40:41'),
(18, 'ganeshkumar', 'ganesh@gmail.com', 'hdjhdj', '2021-07-24 02:40:57', '2021-07-24 02:40:57'),
(19, 'ganeshkumar', 'ganesh@gmail.com', 'hdjhdj', '2021-07-24 02:44:58', '2021-07-24 02:44:58'),
(20, 'vfdv', 'ganesh@gmail.com', 'jjjij', '2021-07-24 02:53:34', '2021-07-24 02:53:34'),
(21, 'vfdv', 'ganesh@gmail.com', 'jjjij', '2021-07-24 02:54:08', '2021-07-24 02:54:08'),
(22, 'vfdv', 'ganesh@gmail.com', 'jjjij', '2021-07-24 03:01:28', '2021-07-24 03:01:28'),
(23, 'vfdv', 'ganesh@gmail.com', 'jjjij', '2021-07-24 03:04:25', '2021-07-24 03:04:25'),
(24, 'ganeshkumar M', 'ganesh@gmail.com', 'i am satisfied from this website', '2021-07-24 03:05:55', '2021-07-24 03:05:55'),
(25, 'dineshkumar M', 'dinesh@gmail.com', 'i happy from your products', '2021-07-24 03:24:24', '2021-07-24 03:24:24');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_07_24_064346_feedback', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `feedbackdetails`
--
ALTER TABLE `feedbackdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `feedbackdetails`
--
ALTER TABLE `feedbackdetails`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
